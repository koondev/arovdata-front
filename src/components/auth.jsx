import {
  Box,
  Container,
  FormControlLabel,
  Checkbox,
  TextField,
  Divider,
  Button,
} from "@material-ui/core";
import { AccountBox, PersonAdd } from "@material-ui/icons";
import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import ParticleBg from "particles-bg";

export const Auth = () => {
  const { name } = useParams();
  const [accept, setAccept] = useState(false);
  const [login, setLogin] = useState(false);
  const [regInput, setRegInput] = useState({
    nom: "",
    prenom: "",
    email: "",
    password: "",
  });
  const [logInput, setLogInput] = useState({ nom_email: "", password: "" });

  let { nom, prenom, email, reg_password } = regInput;
  let { nom_email, log_password } = logInput;

  useEffect(() => {
    if ("connexion" === name) {
      setLogin(true);
    } else {
      setLogin(false);
    }
  }, [name]);

  const handleChange = (e) => {
    const { name, value } = e.target;

    if (login) {
      setLogInput({ ...logInput, [name]: value });
    } else {
      setRegInput({ ...regInput, [name]: value });
    }
  };

  const handleRegister = (e) => {
    e.preventDefault();
  };

  const handleLogin = (e) => {
    e.preventDefault();
  };

  return (
    <Box
      style={{ backgroundColor: "rgb(4, 5, 48)" }}
      position="fixed"
      width="100%"
      height="100%"
      overflow="auto"
    >
      <ParticleBg type="lines" bg num={100} />
      <Container style={{ paddingTop: 30, maxWidth: 500, paddingBottom: 30 }}>
        <Box
          display="flex"
          flexDirection="column"
          padding={2}
          textAlign="center"
          style={{ backgroundColor: "whitesmoke", borderRadius: 15 }}
        >
          <Box>
            {!login ? (
              <PersonAdd style={{ fontSize: 35, color: "maroon" }} />
            ) : (
              <AccountBox style={{ fontSize: 35, color: "blue" }} />
            )}
          </Box>
          <Box>
            <p style={{ fontSize: 30, fontFamily: "acme" }}>
              {login ? "Connexion" : "Inscription"}
            </p>
          </Box>
          <form
            onSubmit={login ? handleLogin : handleRegister}
            style={{ paddingBottom: 20 }}
          >
            {!login && (
              <>
                <TextField
                  name="nom"
                  onChange={handleChange}
                  value={nom}
                  fullWidth
                  label="Nom *"
                  variant="outlined"
                />
                <Box pt={1} pb={1}>
                  <TextField
                    onChange={handleChange}
                    name="prenom"
                    value={prenom}
                    fullWidth
                    label="Prenom *"
                    variant="outlined"
                  />
                </Box>
              </>
            )}
            <Box pb={1}>
              <TextField
                onChange={handleChange}
                name={login ? "name_or_email" : "email"}
                value={login ? nom_email : email}
                fullWidth
                label={login ? "Nom ou Email *" : "Email *"}
                variant="outlined"
              />
            </Box>
            <TextField
              onChange={handleChange}
              name="password"
              value={!login ? reg_password : log_password}
              fullWidth
              variant="outlined"
              type="password"
              label="Mot de passe *"
            />
            {!login && (
              <FormControlLabel
                style={{ fontFamily: "acme" }}
                control={
                  <Checkbox
                    checked={accept}
                    onChange={() => setAccept(!accept)}
                  />
                }
                label={
                  <p>
                    Vous avez lu et accepte les conditions d'utilisations sur
                    cette plateforme .{" "}
                    <i style={{ color: "blue", textDecoration: "underline" }}>
                      voir
                    </i>
                  </p>
                }
              />
            )}
            <Box pt={login ? 1.5 : 0}>
              <Button type="submit" variant="contained" color="primary">
                {!login ? "S'inscrire" : "Connexion"}
              </Button>
            </Box>
          </form>
          <Divider />
          <Box
            display="flex"
            justifyContent="space-between"
            fontFamily="acme"
            pt={2}
            alignItems="center"
          >
            <Box>
              {!login ? "Vous avez deja un compte ?" : "Pas encore inscrit ?"}
              <Link
                style={{ paddingLeft: 10 }}
                to={!login ? "/connexion" : "/inscription"}
              >
                {login ? "S'inscrire" : "Connexion"}
              </Link>
            </Box>
            {login && (
              <p style={{ textAlign: "right", color: "blue" }}>
                Mot de passe oublie ?
              </p>
            )}
          </Box>
        </Box>
      </Container>
    </Box>
  );
};
