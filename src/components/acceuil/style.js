import { makeStyles } from "@material-ui/core";

export const styles = makeStyles(() => ({
  cont: {
    backgroundImage: 'url("/assets/image/back.jpg")',
    backgroundSize: "cover",
  },
  logo: {
    backgroundImage: 'url("/assets/image/logo2.png")',
    backgroundSize: "100% 100%",
    width: "47%",
    height: "70%",
  },
  btn: {
    fontFamily: "acme",
    fontSize: 20,
  },
  p_cont: {
    height: 190,
    perspective: "1000px",
    "&:hover": {
      "& div": {
        transform: "rotateY(180deg)",
      },
    },
  },
  p_inner: {
    position: "relative",
    transition: "transform .8s",
    transformStyle: "preserve-3d",
    width: "100%",
    height: "100%",
  },
  p_fr: {
    backgroundColor: "rgba(255,255,255,.3)",
    position: "absolute",
    width: "100%",
    height: "90%",
    color: "black",
  },
  p_back: {
    position: "absolute",
    backfaceVisibility: "hidden",
    width: "100%",
    height: "90%",
    transform: "rotateY(180deg)",
  },
  icon: {
    fontSize: "40px",
    color: "pink",
  },
  i_cont: {
    backgroundColor: "rgb(245, 56, 56)",
  },
}));
