import { Box, Button, Container, Divider, Fade, Grid } from "@material-ui/core";
import {
  PersonAdd,
  AccountBox,
  ContactSupport,
  Email,
  Facebook,
  LinkedIn,
  Close,
} from "@material-ui/icons";
import { useState } from "react";
import { Blob } from "react-blob";
import { useHistory } from "react-router-dom";
import { styles } from "./style";
import pub from "./pub.json";

export function Home() {
  const classes = styles();
  const [about, setAbout] = useState(false);
  const href = useHistory();

  return (
    <Box
      color="white"
      className={classes.cont}
      position="fixed"
      height="100%"
      width="100%"
    >
      {about && (
        <Fade in={about}>
          <Box
            paddingTop={10}
            onClick={() => setAbout(false)}
            position="fixed"
            top={0}
            left={0}
            zIndex={1}
            width="100%"
            height="100%"
            style={{
              backgroundColor: "rgba(0,0,0,.8)",
            }}
          >
            <Container maxWidth="sm">
              <Box
                onClick={() => setAbout(false)}
                position="absolute"
                top="12%"
                right="30%"
                color="black"
                style={{ cursor: "pointer" }}
              >
                <Close />
              </Box>
              <Box
                style={{
                  backgroundColor: "rgba(255,255,255,.6)",
                  animation: "about .5s",
                }}
                color="black"
                padding={3}
                borderRadius={10}
                a
              >
                <Box display="flex" alignItems="center" justifyContent="center">
                  <p style={{ fontSize: 30, fontFamily: "pangolin" }}>
                    Qui sommes-nous
                  </p>
                  <ContactSupport style={{ fontSize: 65, paddingLeft: 20 }} />
                </Box>
                <Divider />
                <p
                  style={{
                    lineHeight: "25px",
                    fontFamily: "pangolin",
                    fontWeight: "bold",
                  }}
                >
                  Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                  Blanditiis eveniet aliquid dignissimos sunt minus ut
                  consequuntur! Reiciendis, rem! Voluptatibus maxime commodi
                  architecto ea molestiae, reprehenderit quod fugiat minima
                  suscipit eum! Lorem ipsum dolor, sit amet consectetur
                  adipisicing elit. Incidunt sequi, labore iste dolore
                  voluptates modi quasi hic ipsam asperiores rem quibusdam alias
                  tempore cupiditate, praesentium aut recusandae voluptas est
                  blanditiis?
                </p>
                <Box
                  display="flex"
                  paddingTop={2}
                  justifyContent="space-around"
                >
                  <a
                    href="https://facebook.com/danslezone"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <Facebook style={{ fontSize: 50 }} />
                  </a>
                  <a
                    href="https://facebook.com/danslezone"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <LinkedIn style={{ fontSize: 50 }} />
                  </a>
                  <a
                    href="https://facebook.com/danslezone"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <Email style={{ fontSize: 50 }} />
                  </a>
                </Box>
              </Box>
            </Container>
          </Box>
        </Fade>
      )}
      <Container
        maxWidth="sm"
        style={{
          backgroundColor: "grey",
          textAlign: "center",
          padding: 3,
          borderRadius: 5,
          fontFamily: "pangolin",
          fontWeight: "bold",
        }}
      >
        Madagascar , en route vers un developpement technologique
      </Container>
      <Grid container justify="space-between">
        <Grid item xs={12} sm={6} md={6}>
          <Box
            textAlign="center"
            position="absolute"
            paddingLeft={3}
            top={-10}
            className={classes.logo}
          ></Box>
          <Container maxWidth="xs">
            <Box
              display="flex"
              flexDirection="column"
              justifyContent="flex-end"
              alignItems="center"
              height={530}
            >
              <Blob
                size="19.8em"
                style={{
                  backgroundColor: "rgba(0,30,50,.5)",
                }}
                animationDuration="8s"
              >
                <Box padding={5}>
                  <Button
                    className={classes.btn}
                    fullWidth
                    color="secondary"
                    variant="contained"
                    onClick={() => href.push("/inscription")}
                  >
                    <PersonAdd style={{ paddingRight: 10 }} /> S'inscrire
                  </Button>
                  <Box padding={2}>
                    <Button
                      className={classes.btn}
                      fullWidth
                      color="secondary"
                      variant="contained"
                      onClick={() => href.push("/connexion")}
                    >
                      <AccountBox style={{ paddingRight: 10 }} /> Connexion
                    </Button>
                  </Box>
                  <Button
                    className={classes.btn}
                    style={{ width: "110%" }}
                    color="secondary"
                    variant="contained"
                    onClick={setAbout}
                  >
                    <ContactSupport style={{ paddingRight: 10 }} /> Qui
                    sommes-nous ?
                  </Button>
                </Box>
              </Blob>
            </Box>
            <Box display="flex" justifyContent="space-around" paddingTop={3.5}>
              <Blob
                animationDuration="5s"
                className={classes.i_cont}
                size="60px"
              >
                <Facebook className={classes.icon} />
              </Blob>
              <Blob
                animationDuration="5s"
                className={classes.i_cont}
                size="60px"
              >
                <LinkedIn className={classes.icon} />
              </Blob>
              <Blob
                animationDuration="5s"
                className={classes.i_cont}
                size="60px"
              >
                <Email className={classes.icon} />
              </Blob>
            </Box>
          </Container>
        </Grid>

        <Grid item xs={12} sm={6} md={6}>
          <Box paddingRight={1}>
            <p
              style={{
                fontSize: "5.5vh",
                fontFamily: "acme",
                textAlign: "center",
                fontWeight: "bold",
                color: "rgba(0,30,50,.5)",
              }}
            >
              Solution pour la digitalisation et securisation de toutes types de
              dossiers et documents
            </p>
            <Grid container justify="center">
              {pub.map((list, i) => {
                return (
                  <Grid
                    key={i}
                    item
                    style={{ padding: "0px 5px 0px 5px" }}
                    xs={12}
                    sm={6}
                    md={6}
                  >
                    <Box
                      style={{
                        textAlign: "center",
                      }}
                      className={classes.p_cont}
                    >
                      <Box className={classes.p_inner}>
                        <Box className={classes.p_fr}>
                          <Box paddingTop={1}>
                            <img
                              src={list.header}
                              width={100}
                              height={100}
                              alt="pub"
                            />
                          </Box>
                          <Divider />

                          <Box
                            paddingTop={2}
                            style={{ fontFamily: "pangolin", fontSize: 25 }}
                          >
                            {list.p_text}
                          </Box>
                        </Box>
                        <Box
                          textAlign="center"
                          padding={1}
                          fontSize={20}
                          className={classes.p_back}
                          style={{ backgroundColor: list.b_color }}
                        >
                          <p style={{ fontFamily: "pangolin" }}>
                            {list.b_text}
                          </p>
                        </Box>
                      </Box>
                    </Box>
                  </Grid>
                );
              })}
              <Box position="absolute" top="91%" color="black">
                <p
                  style={{
                    fontFamily: "pangolin",
                    fontWeight: "bold",
                    fontSize: 20,
                  }}
                >
                  Copyright © Arovdata 2021
                </p>
              </Box>
            </Grid>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
}
