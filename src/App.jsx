import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Home } from "./components/acceuil/main";
import { Auth } from "./components/auth";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/:name" children={<Auth />} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
